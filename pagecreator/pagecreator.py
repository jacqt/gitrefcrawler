from MoinMoin.web.contexts import ScriptContext
from MoinMoin.Page import Page
from MoinMoin.PageEditor import PageEditor
import json

f_items = open('items.json')
items = json.loads(f_items.read())

def test():
    request = ScriptContext()
    print PageEditor(request, u'TestPage').saveText('Blah', 0)

def addPage(item):
    "parses the item and creates a page from it"
    title = ''.join(item['title'])
    overtitle = ''.join(item['pagetitle'])
    print title
    request = ScriptContext()
    pageText = '{{{#!html \n' + \
            '<h1>{0}</h1>'.format(overtitle  + ' : ' + title ) + '\n'
    #'}}}'
    for contentpiece in item['content']:
        heading = ''.join(contentpiece['heading'])
        subheading = ''.join(contentpiece['subheading'])
        subtext = '<h2>{0}:</h2> <h3>{1}</h3>'.format(heading, subheading) + '\n'
        subtext += ''.join(contentpiece['article'])
        pageText += subtext + '\n'

    pageText += '}}}'
    PageEditor(request, overtitle + ': ' + title).saveText(pageText,0)


if __name__ == '__main__':
    i = 0
    for item in items:
        print i
        addPage(item)
    print 'done'
