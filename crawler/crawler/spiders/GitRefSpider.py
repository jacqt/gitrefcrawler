import scrapy
from crawler.items import SourcePage
from scrapy.contrib.spiders import Rule, CrawlSpider
from scrapy.contrib.linkextractors import LinkExtractor
import string

class GitRefSpider(CrawlSpider):
    name = "gitRef"
    allowed_domains = ["gitref.org"]
    start_urls = [
        "http://gitref.org/index.html"
        ]

    rules = (
            Rule(LinkExtractor(allow=('.+', )), callback='parse_item'),
            )

    def parse_item(self, response):
        item = SourcePage()
        item['pagetitle'] = response.selector.xpath('//title/text()').extract()
        item['url'] = [response.url]
        content = []

        gotTitle = False
        for selector in response.selector.xpath('//div/div[@id="content_box"]/div'):
            if not gotTitle:
                item['title'] = map(lambda x : x.strip(),
                        selector.xpath('.//h2/text()').extract())
                gotTitle = True
                continue
            subcontent = {}
            subcontent['heading'] = map(lambda x : x.strip(),
                    selector.xpath('.//h2/a/text()').extract())
            subcontent['subheading'] = map(lambda x : x.strip(),
                    selector.xpath('.//h2/span[@class="desc"]/text()').extract())
            subcontent['article'] = selector.xpath('.//div').extract()
            content.append(subcontent)
        item['content'] = content
        return item
        
        #item['content'] = 
            #yield item
        #filename = response.url.split("/")[-2]
        #with open(filename, 'wb') as fopen:
            #fopen.write(response.body)
